package com.sulucz.quad;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


/**
 * Created by Adam on 4/20/2016.
 */
public class StickView extends View implements Runnable {
    private static final double RADIAN = 57.2957795;
    private static final int INTERVAL = 100;


    private int xPos;
    private int yPos;

    private int centerX;
    private int centerY;

    private Paint outerCircle;
    private Paint thumbCircle;

    private int thumbRadius;
    private int circleRadius;

    private OnStickMoveListener mOnStickMoveListener;

    private Thread mThread = new Thread(this);

    public StickView(Context ctx){
        super(ctx);
    }

    public StickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StickView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        outerCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
        outerCircle.setColor(Color.GRAY);
        outerCircle.setStyle(Paint.Style.FILL_AND_STROKE);

        thumbCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
        thumbCircle.setColor(Color.BLUE);
        thumbCircle.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onSizeChanged(int newX, int newY, int oldX, int oldY){
        super.onSizeChanged(newX, newY, oldX, oldY);
        xPos = getWidth()/2;
        yPos = getWidth()/2;
        int d = Math.min(newX, newX);
        thumbRadius = (int)(d / 2*0.25);
        circleRadius = (int)(d / 2*0.75);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec){
        int d = Math.min(measure(widthSpec), measure(heightSpec));
        setMeasuredDimension(d, d);
    }

    private int measure(int spec){
        return MeasureSpec.getMode(spec) == MeasureSpec.UNSPECIFIED ? 200 : MeasureSpec.getSize(spec);
    }

    @Override
    protected void onDraw(Canvas canvas){
        centerX = getWidth()/2;
        centerY = getHeight()/2;

        canvas.drawCircle(centerX, centerY, circleRadius, outerCircle);
        canvas.drawCircle(xPos, yPos, thumbRadius, thumbCircle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        xPos = (int)event.getX();
        yPos = (int)event.getY();
        double dist = Math.sqrt( (xPos - centerX)*(xPos - centerX) +
                                 (yPos - centerY)*(yPos - centerY));
        if(dist > circleRadius){
            xPos = (int)((xPos - centerX) * circleRadius / dist + centerX);
            yPos = (int)((yPos - centerY) * circleRadius / dist + centerY);
        }
        invalidate();
        if(event.getAction() == MotionEvent.ACTION_UP){
            xPos = centerX;
            yPos = centerY;
            mThread.interrupt();
            if(mOnStickMoveListener != null){
                mOnStickMoveListener.onPositionChanged((double)(xPos - centerX) / (double)circleRadius,
                        (double)(yPos - centerY) / (double)circleRadius);
            }
        }
        if(event.getAction() == MotionEvent.ACTION_DOWN && mOnStickMoveListener != null){
            if(mThread != null && mThread.isAlive()){
                mThread.interrupt();
            }
            mThread = new Thread(this);
            mThread.start();
            mOnStickMoveListener.onPositionChanged((double)(xPos - centerX) / (double)circleRadius,
                    (double)(yPos - centerY) / (double)circleRadius);
        }
        return true;
    }


    public void setOnStickMoveListener(OnStickMoveListener listener){
        this.mOnStickMoveListener = listener;
    }

    public interface OnStickMoveListener{
        void onPositionChanged(double x, double y);
    }

    @Override
    public void run(){
        while(!Thread.interrupted()){
            post(new Runnable() {
                @Override
                public void run() {
                    if(mOnStickMoveListener != null){
                        mOnStickMoveListener.onPositionChanged((double)(xPos - centerX) / (double)circleRadius,
                                (double)(yPos - centerY) / (double)circleRadius);
                    }
                }
            });
            try{
                Thread.sleep(INTERVAL);
            }
            catch(Exception e){
                break;
            }
        }
    }

}
