package com.sulucz.quad;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Bind(R.id.leftStick) StickView mLeftStickView;
    @Bind(R.id.rightStick) StickView mRightStickView;
    @Bind(R.id.connectButton) AppCompatButton mConnectButton;
    @Bind(R.id.startButton) AppCompatButton mStartButton;
    @Bind(R.id.altHoldButton) AppCompatButton mAltHoldButton;
    @Bind(R.id.calibrateButton) AppCompatButton mCalibrateButton;

    private static final int REQUEST_BT = 1;
    private static final int DATA_WRITE_PERIOD = 40;
    private static int IMU_PERIOD = 0;

    private boolean connected = false;
    private BluetoothService mBluetoothService;
    private String mDeviceName;
    private String mDeviceAddress;
    private Handler mHandler = new Handler();

    private static boolean changedStick = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Intent serviceIntent = new Intent(this, BluetoothService.class);
        bindService(serviceIntent, mServiceConnection, BIND_AUTO_CREATE);
        mHandler.postDelayed(mRunnable, DATA_WRITE_PERIOD);

        mLeftStickView.setOnStickMoveListener(new StickView.OnStickMoveListener() {
            @Override
            public void onPositionChanged(double x, double y) {
                QuadProtocol.Throttle = constrain(1500 - 1000 * y, 1000, 2000);
                QuadProtocol.Yaw = constrain(1500 + 1000 * x, 1000, 2000);
                Log.d(TAG, "Throttle: " + QuadProtocol.Throttle + " Yaw: " + QuadProtocol.Yaw);
                changedStick = true;
            }
        });

        mRightStickView.setOnStickMoveListener(new StickView.OnStickMoveListener() {
            @Override
            public void onPositionChanged(double x, double y) {
                QuadProtocol.Pitch = constrain(1500 + 1000 * y, 1000, 2000);
                QuadProtocol.Roll = constrain(1500 + 1000 * x, 1000, 2000);
                Log.d(TAG, "Pitch: " + QuadProtocol.Pitch + " Roll: " + QuadProtocol.Roll);
                changedStick = true;
            }
        });


        mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!connected) {
                    Log.d(TAG, "Connecting");
                    startActivityForResult(new Intent(MainActivity.this, BTConnectActivity.class), REQUEST_BT);
                }
                else {
                    Log.d(TAG, "Disconnecting");
                    mBluetoothService.disconnect();
                }
            }
        });

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connected){
                    if(mStartButton.getText().toString().equals(getString(R.string.start))){
                        Log.d(TAG, "Start");
                        bluetoothSend(QuadProtocol.getSendData(QuadProtocol.ARM_IT, QuadProtocol.getCommand(QuadProtocol.ARM_IT)));
                        mStartButton.setText(getString(R.string.stop));
                    }
                    else{
                        Log.d(TAG, "Stop");
                        bluetoothSend(QuadProtocol.getSendData(QuadProtocol.DISARM_IT, QuadProtocol.getCommand(QuadProtocol.DISARM_IT)));
                        mStartButton.setText(getString(R.string.start));
                    }
                }
                else{
                    Toast.makeText(MainActivity.this, getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mAltHoldButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connected){
                    if(mAltHoldButton.getCurrentTextColor() == Color.GREEN){
                        Log.d(TAG, "Stop hold alt");
                        bluetoothSend(QuadProtocol.getSendData(QuadProtocol.STOP_HOLD_ALT, QuadProtocol.getCommand(QuadProtocol.STOP_HOLD_ALT)));
                        mAltHoldButton.setTextColor(Color.BLACK);
                    }
                    else{
                        Log.d(TAG, "Hold Alt");
                        bluetoothSend(QuadProtocol.getSendData(QuadProtocol.HOLD_ALT, QuadProtocol.getCommand(QuadProtocol.HOLD_ALT)));
                        mAltHoldButton.setTextColor(Color.GREEN);
                    }
                }
                else{
                    Toast.makeText(MainActivity.this, getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mCalibrateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connected){
                    Log.d(TAG, "Calibrate");
                    bluetoothSend(QuadProtocol.getSendData(QuadProtocol.MSP_ACC_CALIBRATION, QuadProtocol.getCommand(QuadProtocol.MSP_ACC_CALIBRATION)));
                }
                else{
                    Toast.makeText(MainActivity.this, getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onResume(){
        super.onResume();
        registerReceiver(mUpdateReciever, createIntentFilter());
        if(mBluetoothService != null){
            Log.d(TAG, "bluetooth service isn't null");
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        unregisterReceiver(mUpdateReciever);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothService = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == REQUEST_BT && resultCode == RESULT_OK) {
            mDeviceName = data.getExtras().getString(BTConnectActivity.TAG_EXTRAS_NAME);
            mDeviceAddress = data.getExtras().getString(BTConnectActivity.TAG_EXTRAS_ADDRESS);
            if (mBluetoothService != null) {
                mBluetoothService.connect(mDeviceAddress);
            }
        }
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBluetoothService = ((BluetoothService.LocalBinder) service).getService();
            if(!mBluetoothService.initialize()){
                Log.e(TAG, "Unable to initialize bluetooth");
                Toast.makeText(getApplicationContext(), "Unable to initialize bluetooth", Toast.LENGTH_LONG).show();
                finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBluetoothService = null;
        }
    };

    private void bluetoothSend(byte[] data){
        if(connected)
            mBluetoothService.writeCharacteristic(data);
    }

    private static IntentFilter createIntentFilter(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothService.ACTION_GATT_CONNECTED);
        filter.addAction(BluetoothService.ACTION_GATT_DISCONNECTED);
        filter.addAction(BluetoothService.ACTION_GATT_SERVICES_DISCOVERED);
        filter.addAction(BluetoothService.ACTION_DATA_AVAILABLE);
        return filter;
    }

    private final BroadcastReceiver mUpdateReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action){
                case BluetoothService.ACTION_GATT_CONNECTED:
                    connected = true;
                    mConnectButton.setText(R.string.disconnect);
                    break;
                case BluetoothService.ACTION_GATT_DISCONNECTED:
                    connected = false;
                    mConnectButton.setText(R.string.connect);
                    reset();
                    break;
                case BluetoothService.ACTION_GATT_SERVICES_DISCOVERED:
                    mBluetoothService.getSupportedGattServices();
                    break;
                case BluetoothService.ACTION_DATA_AVAILABLE:
                    byte[] data = intent.getByteArrayExtra(BluetoothService.EXTRA_DATA);
                    if(data != null && data.length > 0){
                        StringBuilder sb = new StringBuilder();
                        for(byte b : data){
                            sb.append(String.format("%02X", b));
                        }
                        Log.d(TAG, "Data: " + sb.toString() );
                    }
                    QuadProtocol.processInputData(data, data != null ? data.length : 0);
                    break;
            }
        }
    };

    private void reset(){
        mStartButton.setText(getString(R.string.start));
        mAltHoldButton.setTextColor(Color.BLACK);
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                mHandler.postDelayed(this, DATA_WRITE_PERIOD);
                if (IMU_PERIOD >= 10) {
                    IMU_PERIOD = 0;
                    bluetoothSend(QuadProtocol.getSendData(QuadProtocol.FLY_STATE, QuadProtocol.getCommand(QuadProtocol.FLY_STATE)));
                }
                IMU_PERIOD++;

                if(changedStick){
                    bluetoothSend(QuadProtocol.getSendData(QuadProtocol.SET_4CON, QuadProtocol.getCommand(QuadProtocol.SET_4CON)));
                    changedStick = false;
                }
            }
            catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }
        }
    };

    private int constrain(double num, int low, int high){
        return (int)Math.max(Math.min(num, high), low);
    }
}
