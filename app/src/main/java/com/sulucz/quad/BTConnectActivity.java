package com.sulucz.quad;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BTConnectActivity extends AppCompatActivity {
    private static final String TAG = BTConnectActivity.class.getSimpleName();

    private BluetoothAdapter mBluetoothAdapter;
    private DeviceAdapter mDeviceAdapter;
    private boolean scanning;

    @Bind(R.id.button_cancel) AppCompatButton mCancelButton;
    @Bind(R.id.device_list) ListView mListView;

    private static final int SCAN_DURATION = 10000;
    private static final int BT_ENABLE_REQUEST = 1;

    public static String TAG_EXTRAS_NAME = "name";
    public static String TAG_EXTRAS_ADDRESS = "address";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btconnect);
        ButterKnife.bind(this);

        setTitle(getString(R.string.connect));

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scan(false);
                finish();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BluetoothDevice device = mDeviceAdapter.get(position);
                Intent intent = new Intent();
                intent.putExtra(TAG_EXTRAS_NAME, device.getName());
                intent.putExtra(TAG_EXTRAS_ADDRESS, device.getAddress());
                if(scanning){
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    scanning = false;
                }

                setResult(RESULT_OK, intent);
                finish();
            }
        });

        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)){
            Toast.makeText(this, getString(R.string.err_no_Bluetooth), Toast.LENGTH_LONG).show();
            finish();
        }

        final BluetoothManager manager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();

        if(mBluetoothAdapter == null){
            Toast.makeText(this, getString(R.string.err_no_Bluetooth), Toast.LENGTH_LONG).show();
            finish();
        }

    }

    @Override
    protected void onResume(){
        super.onResume();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, BT_ENABLE_REQUEST);
        }
        mDeviceAdapter = new DeviceAdapter();
        mListView.setAdapter(mDeviceAdapter);
        scan(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == BT_ENABLE_REQUEST && resultCode == Activity.RESULT_CANCELED){
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause(){
        super.onPause();
        scan(false);
        mDeviceAdapter.clear();
    }

    private void scan(boolean enable){
        Log.d(TAG, "scan = " + enable);
        if(scanning == enable)
            return;

        if(enable){
            scanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        }
        else{
            scanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private class DeviceAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> devices;
        private LayoutInflater mLayoutInflater = BTConnectActivity.this.getLayoutInflater();

        public DeviceAdapter(){
            super();
            devices = new ArrayList<>();
        }

        public void add(BluetoothDevice device){
            if(!devices.contains(device))
                devices.add(device);
        }

        public BluetoothDevice get(int position){
            return devices.get(position);
        }

        public void clear(){
            devices.clear();
        }

        @Override
        public int getCount(){
            return devices.size();
        }

        @Override
        public Object getItem(int pos){
            return get(pos);
        }

        @Override
        public long getItemId(int id){
            return id;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder mViewHolder;
            if(convertView == null){
                convertView = mLayoutInflater.inflate(R.layout.bluetooth_device_item, null);
                mViewHolder = new ViewHolder();
                mViewHolder.name = (TextView)convertView.findViewById(R.id.name);
                mViewHolder.address = (TextView)convertView.findViewById(R.id.address);
                convertView.setTag(mViewHolder);
            }
            else{
                mViewHolder = (ViewHolder)convertView.getTag();
            }

            BluetoothDevice device = devices.get(position);
            if(device.getName() != null && !device.getName().isEmpty()){
                mViewHolder.name.setText(device.getName());
            }
            else{
                mViewHolder.name.setText("?");
            }
            mViewHolder.address.setText(device.getAddress());
            return convertView;
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            Log.d(TAG, device.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDeviceAdapter.add(device);
                    mDeviceAdapter.notifyDataSetChanged();
                }
            });
        }
    };

    static class ViewHolder{
        public TextView name;
        public TextView address;
    }



}
