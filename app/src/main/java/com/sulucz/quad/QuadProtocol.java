package com.sulucz.quad;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Adam on 4/15/2016.
 */
public class QuadProtocol {
    private static final String TAG = QuadProtocol.class.getSimpleName();

    public static int Throttle;
    public static int Yaw;
    public static int Pitch;
    public static int Roll;

    public static float pitchAngle;
    public static float rollAngle;
    public static float yawAngle;
    public static float voltage;
    public static float altitude;
    public static float speedZ;

    public static byte[] output;

    public static final int SET_THROTTLE = 0x01;
    public static final int SET_YAW = 0x02;
    public static final int SET_PITCH = 0x03;
    public static final int SET_ROLL = 0x04;
    public static final int ARM_IT = 5;
    public static final int DISARM_IT = 6;
    public static final int SET_4CON = 7;
    public static final int LAUNCH = 8;
    public static final int LAND_DOWN = 9;
    public static final int HOLD_ALT = 10;
    public static final int STOP_HOLD_ALT = 11;
    public static final int HEAD_FREE = 12;
    public static final int STOP_HEAD_FREE = 13;
    public static final int POS_HOLD = 14;
    public static final int STOP_POS_HOLD = 15;
    public static final int FLY_STATE = 16; //pitch、roll、yaw、Altitude、GPS_FIX?、Sat num、Voltage
    public static final int MSP_SET_1WP = 17;
    public static final int SET_MOTOR = 214;
    public static final int MSP_ACC_CALIBRATION = 205;
    public static final int LAUNCH_THROTTLE = 1455;
    public static final int LAND_THROTTLE = 1340;

    public static final int IDLE = 0;
    public static final int HEADER_START = 1;
    public static final int HEADER_M = 2;
    public static final int HEADER_ARROW = 3;
    public static final int HEADER_SIZE = 4;
    public static final int HEADER_CMD = 5;
    public static final int HEADER_ERR = 6;

    private static final String HEADER = "$M<";
    private static boolean frameEnd = false;
    private static int currentState = IDLE;
    private static boolean errorRecieved;
    private static byte checkSum = 0;
    private static byte command;
    private static int offset = 0;
    private static int dataSize = 0;
    private static byte[] buffer = new byte[256];
    private static int p;


    public static byte[] getCommand(int command){
        List<Byte> data = new LinkedList<>();

        switch(command){
            case SET_MOTOR:
            case SET_THROTTLE:
                data.add((byte)((Throttle )&0xff));
                data.add((byte)((Throttle>>8)&0xff));
                break;
            case SET_YAW:
                data.add((byte)((Yaw )&0xff));
                data.add((byte)((Yaw>>8)&0xff));
                break;
            case SET_PITCH:
                data.add((byte)((Pitch )&0xff));
                data.add((byte)((Pitch>>8)&0xff));
                break;
            case SET_ROLL:
                data.add((byte)((Roll )&0xff));
                data.add((byte)((Roll>>8)&0xff));
                break;
            case SET_4CON:
                data.add((byte)((Throttle )&0xff));
                data.add((byte)((Throttle>>8)&0xff));
                data.add((byte)((Yaw )&0xff));
                data.add((byte)((Yaw>>8)&0xff));
                data.add((byte)((Pitch )&0xff));
                data.add((byte)((Pitch>>8)&0xff));
                data.add((byte)((Roll )&0xff));
                data.add((byte)((Roll>>8)&0xff));
                break;
            case ARM_IT:
            case DISARM_IT:
            case LAUNCH:
            case LAND_DOWN:
            case HOLD_ALT:
            case STOP_HOLD_ALT:
            case STOP_HEAD_FREE:
            case HEAD_FREE:
            case POS_HOLD:
            case STOP_POS_HOLD:
            case FLY_STATE:
            case MSP_ACC_CALIBRATION:
                return null;
            default: break;
        }

        byte[] cmd = new byte[data.size()];
        for(int i = 0; i < data.size(); i++){
            cmd[i] = data.get(i);
        }
        return cmd;
    }

    public static byte[] getSendData(int cmd, byte[] data){
        if(cmd < 0)
            return null;

        List<Byte> list = new LinkedList<>();
        for(byte b : HEADER.getBytes()){
            list.add(b);
        }
        byte checksum = 0;
        byte dataSize = (byte)(data != null ? data.length : 0);
        checksum ^= dataSize & 0xff;
        list.add(dataSize);

        list.add((byte)(cmd & 0xff));
        checksum ^= cmd & 0xff;

        if(data != null){
            for(byte b : data){
                list.add((byte)(b & 0xff));
                checksum ^= b & 0xff;
            }
        }
        list.add(checksum);

        byte[] dataToSend = new byte[list.size()];
        for(int i = 0; i < list.size(); i++){
            dataToSend[i] = list.get(i);
        }
        return dataToSend;
    }

    private static int read32(){
        return (buffer[p++] & 0xff) +
                ((buffer[p++] & 0xff) << 8) +
                ((buffer[p++] & 0xff) << 16) +
                ((buffer[p++] & 0xff) << 24);
    }

    private static int read16(){
        return (buffer[p++] & 0xff) +
                ((buffer[p++] & 0xff) << 8);
    }

    private static int read8(){
        return (buffer[p++] & 0xff);
    }

    public static int processInputData(byte[] input, int length){
        int c;
        for(int i = 0; i < length; i++){
            c = input[i];
            if (currentState == IDLE) {//$
                currentState = (c=='$') ? HEADER_START : IDLE;
            }
            else if (currentState == HEADER_START) {//M
                currentState = (c=='M') ? HEADER_M : IDLE;
            }
            else if (currentState == HEADER_M) {
                if (c == '>') {	//right
                    currentState = HEADER_ARROW;
                } else if (c == '!') {
                    currentState = HEADER_ERR;	//wrong
                } else {
                    currentState = IDLE;
                }
            }
            else if (currentState == HEADER_ARROW || currentState == HEADER_ERR) {
                // is this an error message?
                errorRecieved = (currentState == HEADER_ERR);        // now we are expecting the payload size
                dataSize = (c&0xFF);
                // reset index variables
                p = 0;
                offset = 0;
                checkSum = 0;
                checkSum ^= (c&0xFF);
                // the command is to follow
                currentState = HEADER_SIZE;
            }
            else if (currentState == HEADER_SIZE) {
                command = (byte)(c&0xFF);
                checkSum ^= (c&0xFF);
                currentState = HEADER_CMD;
            }
            else if (currentState == HEADER_CMD && offset < dataSize) {
                checkSum ^= (c&0xFF);
                buffer[offset++] = (byte)(c&0xFF);
            }
            else if (currentState == HEADER_CMD && offset >= dataSize) {
                frameEnd=true;
                // compare calculated and transferred checksum
                if ((checkSum & 0xFF) == (c&0xFF)) {
                    if (!errorRecieved) {
                        evaluate(command, dataSize);
                    }
                }
                else
                {
                    System.out.println("invalid checksum for command "+((int)(command&0xFF))+": "+(checkSum&0xFF)+" expected, got "+(int)(c&0xFF));
                    System.out.print("<"+(command&0xFF)+" "+(dataSize&0xFF)+"> {");
                    for (i=0; i<dataSize; i++) {
                        if (i!=0) { System.err.print(' '); }
                        System.out.print((buffer[i] & 0xFF));
                    }
                    System.out.println("} ["+c+"]");
                    System.out.println(new String(buffer, 0, dataSize));
                }
                currentState = IDLE;

            }
        }
        if(!frameEnd)
            return -2;
        frameEnd = false;
        if(errorRecieved)
            return -1;
        return command;

    }

    public static void evaluate(byte command, int dataSize){
        if((command & 0xff) == FLY_STATE){
            rollAngle = read16()/10;
            pitchAngle = read16()/10;
            yawAngle = read16()/10;
            altitude = read32()/100.0f;
            voltage = read16()/100.0f;
            speedZ = read16()/1000.0f;
        }
    }
}

